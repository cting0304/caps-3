import { Button, Row, Col, Card } from 'react-bootstrap';	
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';



export default function CourseCard({courseProp}){

    const {_id, name, description, price} = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isDisabled, setIsDisabled] = useState();
    // Function that keep track of the enrollees
    // The setter function for UseState are asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed

    function enroll(){
        if(seats > 0){
            setCount(count + 1);
            setSeats(seats - 1)
            return
        }
        return alert('No more seats');
    }

    useEffect(() => {
        if(seats === 0){
            setIsDisabled(true);
        }
    }, [seats]);

	return (
		<Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Enrollees</Card.Subtitle>
                <Card.Text>{count} Enrollees</Card.Text>
                <Card.Subtitle>Seats</Card.Subtitle>
                <Card.Text>{seats} Seats</Card.Text>
                <Button as = {Link} to = {`/courses/${_id}`} variant="primary" disabled={isDisabled}>See more details</Button>
            </Card.Body>
        </Card>
	)
}

// CHeck if the CourseCard component is getting the correcct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next 
CourseCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific shape
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}