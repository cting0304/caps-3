import { Fragment, useEffect, useState } from 'react';
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';


export default function Courses(){

	// The course in the CourseCard component is called a prop which is a shorthand for "property" since components are considered as objects in ReactJS
	// The curly braces ({}) are used for props to signify that we are providing information using Javascript expressions rather than hard coded values which use double quotes ""
	// We can pass information from one component to another using props. This is refered to as props drilling

	// The map method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed
	// Everytime the map method loops through the data, it created a "CourseCard" component and then passes the current element in our courseData array using the courseProp
	/*const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
		)
	})*/

	const [courses, setCourses] = useState([])

	// we are goint to add an useEffect here so that in every time that we refresj our application it will fetch the updated content of our courses.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key = {course._id} courseProp = {course} />
				)
			}));
		})
	}, [])	

	return(
		<Fragment>
			{courses}
		</Fragment>
	)
}