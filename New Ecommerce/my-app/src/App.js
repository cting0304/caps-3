import React from 'react';

import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import FeaturedProductView from './components/FeaturedProductView';
import Dashboard from './components/Dashboard';

import {useState, useEffect} from 'react';

import './App.css';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Orders from './pages/Orders';


// import necessary modules from react-router-dom tht will be used for the routing


import { UserProvider } from './UserContext';

import { BrowserRouter, Route, Routes } from 'react-router-dom'; 
import Footer from './components/Footer';

// React JS is a SPA
// Whenever a link is clicked, it functions as if the page is being reloaded but what actually happens is it goes through the process of rendering the page, mounting, rendering and remounting. 


function App() {
  
  const [user, setUser] = useState({
        _id: null,
        isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };
  
  useEffect(()=> {
    console.log(user)
  }, [user]);

 
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if(data.profile && data.profile.user && data.profile.user._id) {
          setUser({
            id: data.profile.user._id,
            isAdmin: data.profile.user.isAdmin,
          });
      }
      });
  }, []);

  useEffect(() => {
    // Remove vertical scroll bar on component mount
    document.documentElement.style.overflowX = 'hidden';

    return () => {
      // Restore vertical scroll bar on component unmount
      document.documentElement.style.overflowX = 'auto';
    };
  }, []);

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>  
      <BrowserRouter>
      <Routes>
      {user.isAdmin ? (
            <Route path="/" element={<PageNotFound/>} />
          ) : (
            <Route path="/" element={
              <>
              <AppNavBar />  
              <Home />
              </>
            
            } 
            />
          )}
        
        <Route
          path="/register"
          element={
            <>
              <AppNavBar />
                <Register />
                <Footer />
            </>
          }
        />
        <Route
          path="/login"
          element={
            <>
              <AppNavBar />   
                <Login />
                <Footer />
            </>
          }
        />
       <Route
          path="/products/:productId"
          element={
            <>
              <AppNavBar />
              <FeaturedProductView />
              <Footer />
            </>
          }
        />
        

        <Route
          path="/logout"
          element={
            <>
              <AppNavBar />
                <Logout />
                <Footer />
            </>
          }
        />

        <Route
          path="/dashboard"
          element={
            user.isAdmin ? (
              
                <Dashboard />
                 
              
            ) : (
              <PageNotFound />
            )
          }
        />

        <Route
          path="/products"
          element={
           
              
              <Products/>
            
          }
        />

        <Route
          path="/cart"
          element={
              <>
              <AppNavBar/>
              
              <Cart />
              <Footer/>
              </>
          }
        />  

          <Route
          path="/orders"
          element={
              <>
              <Orders />
              </>
          }
        />  
       
         
                    
              
        
        <Route path="/*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
    </UserProvider>
  );
}

export default App;
