import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function FeaturedProductView() {
  const [product, setProduct] = useState({});
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [addedAmount, setAddedAmount] = useState(0);
  const { productId } = useParams();
  const [isLoading, setIsLoading] = useState(true); // New state variable

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(response => response.json())
      .then(data => {
        console.log('Fetched data:', data);
        if (data) {
          setProduct(data);
        }
        setIsLoading(false); // Mark loading as complete
      })
      .catch(error => {
        console.error('Error fetching product:', error);
        setIsLoading(false); // Mark loading as complete even if there's an error
      });
  }, [productId]);

  const addToCart = () => {
    const token = localStorage.getItem('token');
    if (!token) {
      // No user is logged in, redirect to the login page
      window.location.href = '/login';
      return;
    }
    
    fetch(`${process.env.REACT_APP_API_URL}/users/cart/add`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        name: product.name,
        description: product.description,
        quantity: quantity,
        price: product.price, // Include the price from the product object
        subtotal: (product.price * quantity).toFixed(2),
      }),
    })
      .then(response => response.json())
      .then(data => {
        if (data.message === 'Item added to cart successfully.') {
          const productAmount = quantity * product.price;
          setAddedAmount(productAmount.toFixed(2));
          setShowSuccessModal(true);
          setTimeout(() => {
            setShowSuccessModal(false);
          }, 5000);
        } else {
          setShowErrorModal(true);
        }
      })
      .catch(error => {
        console.error('Error adding to cart:', error);
        setShowErrorModal(true);
      });
  };
  
  const handleQuantityChange = e => {
    setQuantity(parseInt(e.target.value, 10));
  };

  const handleModalClose = () => {
    setShowSuccessModal(false);
    setShowErrorModal(false);
  };
  if (isLoading) {
    return <div>Loading...</div>;
  }

  const handleViewCart = () => {
    window.location.href = '/cart';
  };
  
  const { name, price, description, imageURL } = product;
  return (
    <section className="cart-item p-5">
      <div className="container-xxl">
        <div className="row ">
          <div className="col-lg-6 justify-content-center align-items-center">
            <img src={imageURL} alt="" className="img-fluid" />
          </div>
          <div className="col-lg-4">
            <div>
              <h2>{name}</h2>
              <h5>{description}</h5>
              <h5>₱{price}</h5>
            </div>
            <div className="quantity-input">
              <label htmlFor="quantity-input">Quantity:</label>
              <input
                type="number"
                id="quantity-input"
                min="1"
                value={quantity}
                onChange={handleQuantityChange}
              />
            </div>
            <button className="add-button btn mt-5" onClick={addToCart}>
              Add to Cart
            </button>
          </div>
        </div>
      </div>
      <Modal show={showSuccessModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Success!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>You have successfully added to your cart.</p>
          <h5>Total</h5>
          <p>₱{addedAmount}</p>
        </Modal.Body>
        <Modal.Footer>
          
          <Button variant="primary" onClick={handleViewCart}>
            View Cart
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showErrorModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Error</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Something went wrong while adding the item to your cart. Please try again.</p>
        </Modal.Body>
      </Modal>
    </section>
  );
}

FeaturedProductView.propTypes = {
  cart: PropTypes.shape({
    _id: PropTypes.string,
    imageURL: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.number,
  }),
};
