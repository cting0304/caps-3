import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

export default function FeaturedProductCard({ product }) {
  const { _id, imageURL, name, description } = product;
  return (
    <div className="col-3 mb-3">
      <NavLink to={`/products/${_id}`} className="card-link link">
        <div className="card text-center">
          <div className="card-details">
            <img src={imageURL} alt={name} className="card-img-top" />
            <div className="card-body">
              <h3 className="card-title">{name}</h3>
              <p className="card-text">{description}</p>
            </div>
          </div>
        </div>
      </NavLink>
    </div>
  );
}

FeaturedProductCard.propTypes = {
  product: PropTypes.shape({
    _id: PropTypes.string,
    imageURL: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
  }),
};
