import React from 'react';


export default function Products(){
    return(
        <section className="product p-2">
        <div className='container-xxl'>
            <div className='row'>
                <div className='product-details d-flex justify-content-around text-center '>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/e6da41fa-1be4-4ce5-b89c-22be4f1f02d4/air-force-1-07-shoes-WrLlWX.png' alt="" className='img-fluid' />
                    </div>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/f52549ad-24d9-4c23-98fa-7052c11e47bc/pegasus-40-road-running-shoes-50CtF7.png' alt="" className='img-fluid' />
                    </div>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/d858287a-d718-482e-804b-3bff435113d5/air-max-pulse-shoes-QShhG8.png' alt="" className='img-fluid' />
                    </div>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/9c7acf78-3f03-4762-ac51-7f71921e40a7/air-max-1-shoes-KqVPww.png' alt="" className='img-fluid' />
                    </div>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/9c7acf78-3f03-4762-ac51-7f71921e40a7/air-max-1-shoes-KqVPww.png' alt="" className='img-fluid' />
                    </div>
                    <div className='card p-2'>
                        <img src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/9c7acf78-3f03-4762-ac51-7f71921e40a7/air-max-1-shoes-KqVPww.png' alt="" className='img-fluid' />
                    </div>

                </div>
            </div>
        </div>
    </section>
    )
}