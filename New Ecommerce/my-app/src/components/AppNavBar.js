import { IoCallOutline, IoSearchOutline } from 'react-icons/io5';
import { FiMail } from 'react-icons/fi';
import { BsFacebook, BsTwitter, BsInstagram, BsHeart, BsCart } from 'react-icons/bs';
import Button from 'react-bootstrap/Button';
import { NavLink, Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import { useContext } from 'react';
import UserContext from '../UserContext';
import logo from '../images/logo.png';
import React from 'react';




export default function AppNavBar() {
  
  const { user } = useContext(UserContext);

 
  return (
    <>
      <header className="header-top-strip shadow-sm">
        <div className="container-xxl">
          <div className="header-content">
            <div className="left-content">
              <div className="icon-container">
                <IoCallOutline className="icon" />
                <a href="tel:+649685934336" className="contact-link">
                  09685934336
                </a>
              </div>
              <div className="icon-container">
                <FiMail className="icon" />
                <a href="mailto:justforkicks@gmail.com" className="mail-link">
                  justforkicks@gmail.com
                </a>
              </div>
            </div>
            <div className="right-content">
              <div className="icon-container">
                <a href="https://facebook.com/justforkicks" target="_blank" className="icon">
                  <BsFacebook size={25} />
                </a>
                <a href="https://instagram.com/justforkicks" target="_blank" className="icon">
                  <BsInstagram size={25} />
                </a>
                <a href="https://twitter.com/justforkicks" target="_blank" className="icon">
                  <BsTwitter size={25} />
                </a>
              </div>
             
              {
                user.id === null || user.id === undefined 
                ? 
                
                  <>
                  <Button className="custom-button btn" as={NavLink} to="/register">
                    Register
                  </Button>
                  <Button className="custom-button btn" as={NavLink} to="/login">
                    Sign In
                  </Button>
                </>
                : 
                <div className="button-container btn small-button">
                <Button className="custom-button btn" as={NavLink} to="/logout">
                  Logout
                </Button>
              </div>
              
            }

            
            </div>
          </div>
        </div>
      </header>

      <header className="header-upper sticky-top">
        <div className="container-xxl">
          <div className="row">
            <div className="col-2">
              <Link to="/">
                <img src={logo} alt="" className="img-fluid custom-logo" />
              </Link>
            </div>
            <div className="col-6">
              <div className="navlist">
                <Link to="/men" className="navlink">
                  MEN
                </Link>
                <Link to="/women" className="navlink">
                  WOMEN
                </Link>
                <Link to="/kids" className="navlink">
                  KIDS
                </Link>
              </div>
            </div>
            <div className="col-4 d-flex align-items-center justify-content-end">
              <div className="search-container">
                <Form className="search">
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                  />
                  <Button className="custom-button btn">Search</Button>
                  <div className="icon-container">
                      <Link to="/wishlist">
                        <BsHeart className="icon" />
                      </Link>
                      <Link to="/cart">
                        <BsCart className="icon" />
                      </Link>
                    </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
