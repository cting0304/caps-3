import { Form, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  const { user, setUser } = useContext(UserContext);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then(response => response.json())
      .then(data => {
        if (data === false) {
          Swal.fire({
            title: 'Login unsuccessful!',
            icon: 'error',
            text: 'User does not exist. Please check your login credentials and try again.',
          });
        } else {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);
        }
      });
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        setUser({
          id: data.profile.user._id,
          isAdmin: data.profile.user.isAdmin,
        });

        if (data.profile.user.isAdmin) {
          Swal.fire({
            title: 'Login successful!',
            icon: 'success',
            text: 'Welcome Admin!',
          });
          navigate('/dashboard');
        } else {
          Swal.fire({
            title: 'Login successful!',
            icon: 'success',
            text: 'Welcome to JustForKicks!',
          });
          navigate('/');
        }
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [email, password]);

  return (
    user.id === null || user.id === undefined ? 
      <Row>
        <Col className="col-6 mx-auto d-flex align-items-center justify-content-center login-form">
          <div className="col-8 p-3">
            <h1>Login</h1>
            <p>If you are already a member, easily log in</p>
          </div>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail" className="mb-4">
              <Form.Control
                type="email"
                placeholder="Enter email"
                className="input custom-input"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="password" className="mb-4">
              <Form.Control
                type="password"
                placeholder="Password"
                className="input custom-input"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="keepSignedIn" className="mb-4 checkbox">
              <div className="d-flex justify-content-between">
                <Form.Check type="checkbox" label="Keep me signed in" />
                <a href="#" className="forgot-password-link">
                  Forgot your password?
                </a>
              </div>
            </Form.Group>
            <Button
              type="submit"
              id="submitBtn"
              className="login-button"
              disabled={isActive}
            >
              Login
            </Button>
          </Form>
          {/* <div className="col-6 mx-auto d-flex align-items-center justify-content-center">
            <hr className="header-top-strip shadow-sm mt-4"></hr>
            <p className="mt-2">OR</p>
            <hr className="header-top-strip shadow-sm mt-4"></hr>
          </div> */}
        </Col>
      </Row>
     : 
      <Navigate to="/*" />
  );
}