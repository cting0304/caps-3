let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    let newCollection = [];
    for (let i = 0; i < collection.length; i++) {
        newCollection[i] = collection[i];
    }
    
    newCollection[collection.length] = element;
    collection = newCollection;
    
    return collection;
}


function dequeue() {
    // In here you are going to remove the first element in the array
    let newCollection = [];

    for(let i = 1; i < size(); i++){
        newCollection[i - 1] = collection[i]
    }
    collection = newCollection;
    return collection;

}

function front() {
    // you will get the first element
    return collection[0];
}


function size() {
     // Number of elements   
    let count = 0;
    for (let i = 0; collection[i]; i++) {  
        count++;
    }
    return count;        
}

function isEmpty() {
    //it will check whether the function is empty or not
    if(size() == 0) {
        return true;
    }
    return false;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};